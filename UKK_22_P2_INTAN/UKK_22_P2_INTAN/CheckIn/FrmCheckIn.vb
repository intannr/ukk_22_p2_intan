﻿Imports System.Data.SqlClient
Public Class FrmCheckIn
    Dim cn As New SqlConnection
    Dim cmd As New SqlCommand
    Sub tampildata()
        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "SELECT * FROM check_in"
        Dim rd As SqlDataReader = cmd.ExecuteReader
        Dim dt As New DataTable
        dt.Load(rd)
        DataGridView1.DataSource = dt
        cn.Close()

        DataGridView1.Columns(0).HeaderText = "ID Check In"
        DataGridView1.Columns(1).HeaderText = "ID Pemesanan"
        DataGridView1.Columns(2).HeaderText = "Nama Tamu"
        DataGridView1.Columns(3).HeaderText = "Tanggal Check In"

        DataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill

    End Sub

    Sub bersih()
        txtidci.Text = ""
        txtidpms.Text = ""
        txtnamat.Text = ""
    End Sub

    Sub kodeotomatis()
        Dim kodeauto As Single
        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "SELECT COUNT(*) AS id_checkin FROM check_in"
        Dim rd As SqlDataReader = cmd.ExecuteReader
        While rd.Read
            kodeauto = Val(rd.Item("id_checkin").ToString) + 1
        End While
        Select Case Len(Trim(kodeauto))
            Case 1 : txtidci.Text = "CI00" + Trim(Str(kodeauto))
            Case 2 : txtidci.Text = "CI-" + Trim(Str(kodeauto))
        End Select
        rd.Close()
        cn.Close()
    End Sub

    Sub id_pemesanan()
        Try
            Dim dt As New DataTable
            Dim ds As New DataSet
            ds.Tables.Add(dt)
            Dim da As New SqlDataAdapter("SELECT * FROM pemesanan", cn)
            da.Fill(dt)
            Dim r As DataRow
            txtidpms.AutoCompleteCustomSource.Clear()
            For Each r In dt.Rows
                txtidpms.AutoCompleteCustomSource.Add(r.Item(0).ToString)
            Next
        Catch ex As Exception
            cn.Close()
        End Try
    End Sub

    Private Sub FrmCheckIn_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cn.ConnectionString = "Data Source=DESKTOP-GCB5DI9\SQLEXPRESS;Initial Catalog=db_22_P2_Intan;Integrated Security=True"
        tampildata()
        txtidci.Enabled = False
        kodeotomatis()
        id_pemesanan()
    End Sub

    Private Sub txtidpms_TextChanged(sender As Object, e As EventArgs) Handles txtidpms.TextChanged
        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "SELECT * FROM pemesanan WHERE id_pemesanan = '" & txtidpms.Text & "'"
        cmd.ExecuteNonQuery()
        Dim rd As SqlDataReader = cmd.ExecuteReader
        rd.Read()
        If rd.HasRows Then
            txtnamat.Text = rd.Item("nama_tamu")
        Else
        End If
        rd.Close()
        cn.Close()
    End Sub

    Private Sub DataGridView1_DoubleClick(sender As Object, e As EventArgs) Handles DataGridView1.DoubleClick
        txtidci.Text = DataGridView1.SelectedCells(0).Value
        txtidpms.Text = DataGridView1.SelectedCells(1).Value
        txtnamat.Text = DataGridView1.SelectedCells(2).Value
        tglci.Value = DataGridView1.SelectedCells(3).Value
    End Sub

    Private Sub btntambah_Click(sender As Object, e As EventArgs) Handles btntambah.Click
        If txtidci.Text = "" Then
            MessageBox.Show("ID Check In, tidak boleh dikosongkan")
        ElseIf txtidpms.Text = "" Then
            MessageBox.Show("ID Pemesanan wajib diisi, tidak boleh dikosongkan")
        ElseIf txtidci.Text <> "" And txtidpms.Text <> "" Then
            cn.Open()
            cmd.Connection = cn
            cmd.CommandText = "INSERT INTO check_in VALUES ('" & txtidci.Text & "','" & txtidpms.Text & "','" & txtnamat.Text & "','" & tglci.Value.ToString("yyyy-MM-dd") & "')"
            cmd.ExecuteNonQuery()
            cn.Close()
            bersih()
            MsgBox("Data Check In Berhasil Tersimpan", MsgBoxStyle.Information)
            tampildata()
            kodeotomatis()
        End If
    End Sub

    Private Sub btnubah_Click(sender As Object, e As EventArgs) Handles btnubah.Click
        If txtidci.Text = "" Then
            MessageBox.Show("ID Check In, tidak boleh dikosongkan")
        ElseIf txtidpms.Text = "" Then
            MessageBox.Show("No Pemesanan wajib diisi, tidak boleh dikosongkan")
        ElseIf txtidci.Text <> "" And txtidpms.Text <> "" Then
            cn.Open()
            cmd.Connection = cn
            cmd.CommandText = "UPDATE check_in SET id_pemesanan ='" & txtidpms.Text & "', nama_tamu ='" & txtnamat.Text & "', check_in ='" & tglci.Value.ToString("yyyy-MM-dd") & "' WHERE id_checkin ='" & txtidci.Text & "'"
            cmd.ExecuteNonQuery()
            cn.Close()
            bersih()
            MsgBox("Data Check In Berhasil Terubah", MsgBoxStyle.Information)
            tampildata()
            kodeotomatis()
        End If
    End Sub

    Private Sub btnbatal_Click(sender As Object, e As EventArgs) Handles btnbatal.Click
        bersih()
        kodeotomatis()
    End Sub

    Private Sub btnhapus_Click(sender As Object, e As EventArgs) Handles btnhapus.Click
        Dim baris As Integer
        Dim id As String

        baris = DataGridView1.CurrentCell.RowIndex
        id = DataGridView1(0, baris).Value.ToString

        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "DELETE FROM check_in WHERE id_checkin ='" + id + "'"
        cmd.ExecuteNonQuery()
        cn.Close()
        MsgBox("Data Check In Berhasil Terhapus", MsgBoxStyle.Information)
        tampildata()
        kodeotomatis()
    End Sub

    Private Sub txtpncr_TextChanged(sender As Object, e As EventArgs) Handles txtpncr.TextChanged
        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "SELECT * FROM check_in WHERE id_checkin LIKE '%" & txtpncr.Text & "%' OR id_pemesanan LIKE '%" & txtpncr.Text & "%' OR nama_tamu LIKE '%" & txtpncr.Text & "%' OR check_in LIKE '%" & txtpncr.Text & "%'"
        Dim rd As SqlDataReader = cmd.ExecuteReader
        Dim dt As New DataTable
        dt.Load(rd)
        DataGridView1.DataSource = dt
        cn.Close()
    End Sub

    Private Sub btnlaporan_Click(sender As Object, e As EventArgs) Handles btnlaporan.Click
        Me.Hide()
        LaporanCheckIn.Show()
    End Sub
End Class