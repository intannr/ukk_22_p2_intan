﻿Imports System.Data.SqlClient
Public Class FrmCheckOut
    Dim cn As New SqlConnection
    Dim cmd As New SqlCommand
    Sub tampildata()
        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "SELECT * FROM check_out"
        Dim rd As SqlDataReader = cmd.ExecuteReader
        Dim dt As New DataTable
        dt.Load(rd)
        DataGridView1.DataSource = dt
        cn.Close()

        DataGridView1.Columns(0).HeaderText = "ID Check Out"
        DataGridView1.Columns(1).HeaderText = "ID Check In"
        DataGridView1.Columns(2).HeaderText = "Nama Tamu"
        DataGridView1.Columns(3).HeaderText = "Tanggal Check In"
        DataGridView1.Columns(4).HeaderText = "Tanggal Check Out"
        DataGridView1.Columns(5).HeaderText = "Jumlah Hari"

        DataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill

    End Sub

    Sub bersih()
        txtidco.Text = ""
        txtidci.Text = ""
        txtnamat.Text = ""
        txtjmlhri.Text = ""

    End Sub

    Sub kodeotomatis()
        Dim kodeauto As Single
        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "SELECT COUNT(*) AS id_checkout FROM check_out"
        Dim rd As SqlDataReader = cmd.ExecuteReader
        While rd.Read
            kodeauto = Val(rd.Item("id_checkout").ToString) + 1
        End While
        Select Case Len(Trim(kodeauto))
            Case 1 : txtidco.Text = "CO00" + Trim(Str(kodeauto))
            Case 2 : txtidco.Text = "CO-" + Trim(Str(kodeauto))
        End Select
        rd.Close()
        cn.Close()
    End Sub

    Sub id_checkin()
        Try
            Dim dt As New DataTable
            Dim ds As New DataSet
            ds.Tables.Add(dt)
            Dim da As New SqlDataAdapter("SELECT * FROM check_in", cn)
            da.Fill(dt)
            Dim r As DataRow
            txtidci.AutoCompleteCustomSource.Clear()
            For Each r In dt.Rows
                txtidci.AutoCompleteCustomSource.Add(r.Item(0).ToString)
            Next
        Catch ex As Exception
            cn.Close()
        End Try
    End Sub

    Private Sub FrmCheckOut_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cn.ConnectionString = "Data Source=DESKTOP-GCB5DI9\SQLEXPRESS;Initial Catalog=db_22_P2_Intan;Integrated Security=True"
        tampildata()
        txtidco.Enabled = False
        kodeotomatis()
        tglcheckout.Text = Today
        id_checkin()
    End Sub

    Private Sub txtidci_TextChanged(sender As Object, e As EventArgs) Handles txtidci.TextChanged
        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "SELECT * FROM check_in WHERE id_checkin = '" & txtidci.Text & "'"
        cmd.ExecuteNonQuery()
        Dim rd As SqlDataReader = cmd.ExecuteReader
        rd.Read()
        If rd.HasRows Then
            txtnamat.Text = rd.Item("nama_tamu")
            tglcheckin.Text = rd.Item("check_in")
        Else
        End If
        rd.Close()
        cn.Close()

    End Sub

    Private Sub DataGridView1_DoubleClick(sender As Object, e As EventArgs) Handles DataGridView1.DoubleClick
        txtidco.Text = DataGridView1.SelectedCells(0).Value
        txtidci.Text = DataGridView1.SelectedCells(1).Value
        txtnamat.Text = DataGridView1.SelectedCells(2).Value
        tglcheckin.Value = DataGridView1.SelectedCells(3).Value
        tglcheckout.Value = DataGridView1.SelectedCells(4).Value
        txtjmlhri.Text = DataGridView1.SelectedCells(5).Value
    End Sub

    Private Sub btntambah_Click(sender As Object, e As EventArgs) Handles btntambah.Click
        If txtidco.Text = "" Then
            MessageBox.Show("ID Check out, tidak boleh dikosongkan")
        ElseIf txtidci.Text = "" Then
            MessageBox.Show("ID Pemesanan wajib diisi, tidak boleh dikosongkan")
        ElseIf txtidco.Text <> "" And txtidci.Text <> "" Then
            cn.Open()
            cmd.Connection = cn
            cmd.CommandText = "INSERT INTO check_out VALUES ('" & txtidco.Text & "','" & txtidci.Text & "','" & txtnamat.Text & "','" & tglcheckin.Value.ToString("yyyy-MM-dd") & "','" & tglcheckout.Value.ToString("yyyy-MM-dd") & "','" & txtjmlhri.Text & "')"
            cmd.ExecuteNonQuery()
            cn.Close()
            bersih()
            MsgBox("Data Check Out Berhasil Tersimpan", MsgBoxStyle.Information)
            tampildata()
            kodeotomatis()
        End If
    End Sub

    Private Sub btnubah_Click(sender As Object, e As EventArgs) Handles btnubah.Click
        If txtidco.Text = "" Then
            MessageBox.Show("ID Check Out, tidak boleh dikosongkan")
        ElseIf txtidci.Text = "" Then
            MessageBox.Show("No Pemesanan wajib diisi, tidak boleh dikosongkan")
        ElseIf txtidco.Text <> "" And txtidci.Text <> "" Then
            cn.Open()
            cmd.Connection = cn
            cmd.CommandText = "UPDATE check_out SET id_checkin ='" & txtidci.Text & "', nama_tamu ='" & txtnamat.Text & "', check_in ='" & tglcheckin.Value.ToString("yyyy-MM-dd") & "', check_out ='" & tglcheckout.Value.ToString("yyyy-MM-dd") & "', jumlah_hari ='" & txtjmlhri.Text & "' WHERE id_checkout ='" & txtidco.Text & "'"
            cmd.ExecuteNonQuery()
            cn.Close()
            bersih()
            MsgBox("Data Check Out Berhasil Terubah", MsgBoxStyle.Information)
            tampildata()
            kodeotomatis()
        End If
    End Sub

    Private Sub btnbatal_Click(sender As Object, e As EventArgs) Handles btnbatal.Click
        bersih()
        kodeotomatis()
    End Sub

    Private Sub btnhapus_Click(sender As Object, e As EventArgs) Handles btnhapus.Click
        Dim baris As Integer
        Dim id As String

        baris = DataGridView1.CurrentCell.RowIndex
        id = DataGridView1(0, baris).Value.ToString

        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "DELETE FROM check_out WHERE id_checkout ='" + id + "'"
        cmd.ExecuteNonQuery()
        cn.Close()
        MsgBox("Data Check Out Berhasil Terhapus", MsgBoxStyle.Information)
        tampildata()
        kodeotomatis()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Hide()
        LaporanCheckOut.Show()
    End Sub

    Private Sub txtpncr_TextChanged(sender As Object, e As EventArgs) Handles txtpncr.TextChanged
        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "SELECT * FROM check_out WHERE id_checkout LIKE '%" & txtpncr.Text & "%' OR id_checkin LIKE '%" & txtpncr.Text & "%' OR nama_tamu LIKE '%" & txtpncr.Text & "%' OR check_in LIKE '%" & txtpncr.Text & "%' OR check_out LIKE '%" & txtpncr.Text & "%'"
        Dim rd As SqlDataReader = cmd.ExecuteReader
        Dim dt As New DataTable
        dt.Load(rd)
        DataGridView1.DataSource = dt
        cn.Close()
    End Sub

    Private Sub tglcheckout_ValueChanged(sender As Object, e As EventArgs) Handles tglcheckout.ValueChanged
    
        If tglcheckout.Value < tglcheckin.Value Then
            tglcheckout.Value = tglcheckin.Value
        Else
            txtjmlhri.Text = DateDiff(DateInterval.Day, CDate(tglcheckin.Text), CDate(tglcheckout.Text))
        End If
    End Sub

    Private Sub DateTimePicker1_ValueChanged(sender As Object, e As EventArgs) Handles tglcheckin.ValueChanged
        If tglcheckin.Value > tglcheckout.Value Then
            tglcheckin.Value = tglcheckout.Value
        Else
            txtjmlhri.Text = DateDiff(DateInterval.Day, CDate(tglcheckin.Text), CDate(tglcheckout.Text))
        End If

    End Sub
End Class