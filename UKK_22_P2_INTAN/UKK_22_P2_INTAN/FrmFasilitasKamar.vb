﻿Imports System.Data.SqlClient
Public Class FrmFasilitasKamar
    Dim cn As New SqlConnection
    Dim cmd As New SqlCommand
    Sub tampildata()
        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "SELECT * FROM fasilitas_kamar"
        Dim rd As SqlDataReader = cmd.ExecuteReader
        Dim dt As New DataTable
        dt.Load(rd)
        DataGridView1.DataSource = dt
        cn.Close()

        DataGridView1.Columns(0).HeaderText = "ID Fasilitas"
        DataGridView1.Columns(1).HeaderText = "ID Kamar"
        DataGridView1.Columns(2).HeaderText = "Tipe Kamar"
        DataGridView1.Columns(3).HeaderText = "Fasilitas Kamar"

        DataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill

    End Sub

    Sub bersih()
        txtidfs.Text = ""
        txtid.Text = ""
        txttipe.Text = ""
        txtfs.Text = ""
    End Sub

    Sub kodeotomatis()
        Dim kodeauto As Single
        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "SELECT COUNT(*) AS id_fasilitas FROM fasilitas_kamar"
        Dim rd As SqlDataReader = cmd.ExecuteReader
        While rd.Read
            kodeauto = Val(rd.Item("id_fasilitas").ToString) + 1
        End While
        Select Case Len(Trim(kodeauto))
            Case 1 : txtidfs.Text = "FK0" + Trim(Str(kodeauto))
            Case 2 : txtidfs.Text = "FK" + Trim(Str(kodeauto))
        End Select
        rd.Close()
        cn.Close()
    End Sub

    Sub id_kamar()
        Try
            Dim dt As New DataTable
            Dim ds As New DataSet
            ds.Tables.Add(dt)
            Dim da As New SqlDataAdapter("SELECT * FROM kamar", cn)
            da.Fill(dt)
            Dim r As DataRow
            txtid.AutoCompleteCustomSource.Clear()
            For Each r In dt.Rows
                txtid.AutoCompleteCustomSource.Add(r.Item(0).ToString)
            Next
        Catch ex As Exception
            cn.Close()
        End Try
    End Sub

    Private Sub FrmFasilitasKamar_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cn.ConnectionString = "Data Source=DESKTOP-GCB5DI9\SQLEXPRESS;Initial Catalog=db_22_P2_Intan;Integrated Security=True"
        tampildata()
        txtidfs.Enabled = False
        kodeotomatis()
        id_kamar()
    End Sub
    Private Sub DataGridView1_DoubleClick(sender As Object, e As EventArgs) Handles DataGridView1.DoubleClick
        txtidfs.Text = DataGridView1.SelectedCells(0).Value
        txtid.Text = DataGridView1.SelectedCells(1).Value
        txttipe.Text = DataGridView1.SelectedCells(2).Value
        txtfs.Text = DataGridView1.SelectedCells(3).Value
    End Sub

    Private Sub txtid_TextChanged(sender As Object, e As EventArgs) Handles txtid.TextChanged
        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "SELECT * FROM kamar WHERE id_kamar = '" & txtid.Text & "'"
        cmd.ExecuteNonQuery()
        Dim rd As SqlDataReader = cmd.ExecuteReader
        rd.Read()
        If rd.HasRows Then
            txttipe.Text = rd.Item("tipe_kamar")
        Else
        End If
        rd.Close()
        cn.Close()
    End Sub

    Private Sub btntambah_Click(sender As Object, e As EventArgs) Handles btntambah.Click
        If txtidfs.Text = "" Then
            MessageBox.Show("ID Fasilitas Kamar, tidak boleh dikosongkan")
        ElseIf txttipe.Text = "" Then
            MessageBox.Show("Tipe Kamar wajib diisi, tidak boleh dikosongkan")
        ElseIf txtidfs.Text <> "" And txttipe.Text <> "" Then
            cn.Open()
            cmd.Connection = cn
            cmd.CommandText = "INSERT INTO fasilitas_kamar VALUES ('" & txtidfs.Text & "','" & txtid.Text & "','" & txttipe.Text & "','" & txtfs.Text & "')"
            cmd.ExecuteNonQuery()
            cn.Close()
            bersih()
            MsgBox("Data Fasilitas Kamar Berhasil Tersimpan", MsgBoxStyle.Information)
            tampildata()
            kodeotomatis()
        End If
    End Sub

    Private Sub btnubah_Click(sender As Object, e As EventArgs) Handles btnubah.Click
        If txtidfs.Text = "" Then
            MessageBox.Show("ID Fasilitas Kamar, tidak boleh dikosongkan")
        ElseIf txttipe.Text = "" Then
            MessageBox.Show("Tipe Kamar wajib diisi, tidak boleh dikosongkan")
        ElseIf txtidfs.Text <> "" And txttipe.Text <> "" Then
            cn.Open()
            cmd.Connection = cn
            cmd.CommandText = "UPDATE fasilitas_kamar SET id_kamar ='" & txtid.Text & "', tipe_kamar ='" & txttipe.Text & "', fasilitas_kamar = '" & txtfs.Text & "' WHERE id_fasilitas ='" & txtidfs.Text & "'"
            cmd.ExecuteNonQuery()
            cn.Close()
            bersih()
            MsgBox("Data Fasilitas Kamar Berhasil Terubah", MsgBoxStyle.Information)
            tampildata()
            kodeotomatis()
        End If
    End Sub

    Private Sub btnbatal_Click(sender As Object, e As EventArgs) Handles btnbatal.Click
        bersih()
        kodeotomatis()
    End Sub

    Private Sub btnhapus_Click(sender As Object, e As EventArgs) Handles btnhapus.Click
        Dim baris As Integer
        Dim id As String

        baris = DataGridView1.CurrentCell.RowIndex
        id = DataGridView1(0, baris).Value.ToString

        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "DELETE FROM fasilitas_kamar WHERE id_fasilitas ='" + id + "'"
        cmd.ExecuteNonQuery()
        cn.Close()
        MsgBox("Data Fasilitas kamar Berhasil Terhapus", MsgBoxStyle.Information)
        tampildata()
        kodeotomatis()
    End Sub

    Private Sub txtpncr_TextChanged(sender As Object, e As EventArgs) Handles txtpncr.TextChanged
        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "SELECT * FROM fasilitas_kamar WHERE id_fasilitas LIKE '%" & txtpncr.Text & "%' OR tipe_kamar LIKE '%" & txtpncr.Text & "%'"
        Dim rd As SqlDataReader = cmd.ExecuteReader
        Dim dt As New DataTable
        dt.Load(rd)
        DataGridView1.DataSource = dt
        cn.Close()
    End Sub
End Class