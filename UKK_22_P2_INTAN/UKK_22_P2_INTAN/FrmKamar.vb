﻿Imports System.Data.SqlClient
Public Class FrmKamar
    Dim cn As New SqlConnection
    Dim cmd As New SqlCommand
    Sub tampildata()
        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "SELECT * FROM kamar"
        Dim rd As SqlDataReader = cmd.ExecuteReader
        Dim dt As New DataTable
        dt.Load(rd)
        DataGridView1.DataSource = dt
        cn.Close()

        DataGridView1.Columns(0).HeaderText = "ID Kamar"
        DataGridView1.Columns(1).HeaderText = "Tipe Kamar"
        DataGridView1.Columns(2).HeaderText = "Jumlah Kamar"

        DataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill

    End Sub

    Sub bersih()
        txtid.Text = ""
        txttipe.Text = ""
        txtjml.Text = ""
    End Sub

    Sub kodeotomatis()
        Dim kodeauto As Single
        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "SELECT COUNT(*) AS id_kamar FROM kamar"
        Dim rd As SqlDataReader = cmd.ExecuteReader
        While rd.Read
            kodeauto = Val(rd.Item("id_kamar").ToString) + 1
        End While
        Select Case Len(Trim(kodeauto))
            Case 1 : txtid.Text = "KMR0" + Trim(Str(kodeauto))
            Case 2 : txtid.Text = "KMR" + Trim(Str(kodeauto))
        End Select
        rd.Close()
        cn.Close()
    End Sub

    Private Sub FrmKamar_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cn.ConnectionString = "Data Source=DESKTOP-GCB5DI9\SQLEXPRESS;Initial Catalog=db_22_P2_Intan;Integrated Security=True"
        tampildata()
        txtid.Enabled = False
        kodeotomatis()
    End Sub
    Private Sub DataGridView1_DoubleClick(sender As Object, e As EventArgs) Handles DataGridView1.DoubleClick
        txtid.Text = DataGridView1.SelectedCells(0).Value
        txttipe.Text = DataGridView1.SelectedCells(1).Value
        txtjml.Text = DataGridView1.SelectedCells(2).Value
    End Sub

    Private Sub btntambah_Click(sender As Object, e As EventArgs) Handles btntambah.Click
        If txtid.Text = "" Then
            MessageBox.Show("ID kamar, tidak boleh dikosongkan")
        ElseIf txttipe.Text = "" Then
            MessageBox.Show("Tipe Kamar wajib diisi, tidak boleh dikosongkan")
        ElseIf txtid.Text <> "" And txttipe.Text <> "" Then
            cn.Open()
            cmd.Connection = cn
            cmd.CommandText = "INSERT INTO kamar VALUES ('" & txtid.Text & "','" & txttipe.Text & "','" & txtjml.Text & "')"
            cmd.ExecuteNonQuery()
            cn.Close()
            bersih()
            MsgBox("Data Kamar Berhasil Tersimpan", MsgBoxStyle.Information)
            tampildata()
            kodeotomatis()
        End If
    End Sub

    Private Sub btnubah_Click(sender As Object, e As EventArgs) Handles btnubah.Click
        If txtid.Text = "" Then
            MessageBox.Show("ID kamar, tidak boleh dikosongkan")
        ElseIf txttipe.Text = "" Then
            MessageBox.Show("Tipe Kamar wajib diisi, tidak boleh dikosongkan")
        ElseIf txtid.Text <> "" And txttipe.Text <> "" Then
            cn.Open()
            cmd.Connection = cn
            cmd.CommandText = "UPDATE kamar SET tipe_kamar ='" & txttipe.Text & "', jumlah_kamar = '" & txtjml.Text & "' WHERE id_kamar ='" & txtid.Text & "'"
            cmd.ExecuteNonQuery()
            cn.Close()
            bersih()
            MsgBox("Data Kamar Berhasil Terubah", MsgBoxStyle.Information)
            tampildata()
            kodeotomatis()
        End If
    End Sub

    Private Sub btnbatal_Click(sender As Object, e As EventArgs) Handles btnbatal.Click
        bersih()
        kodeotomatis()
    End Sub

    Private Sub btnhapus_Click(sender As Object, e As EventArgs) Handles btnhapus.Click
        Dim baris As Integer
        Dim id As String

        baris = DataGridView1.CurrentCell.RowIndex
        id = DataGridView1(0, baris).Value.ToString

        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "DELETE FROM kamar WHERE id_kamar ='" + id + "'"
        cmd.ExecuteNonQuery()
        cn.Close()
        MsgBox("Data kamar Berhasil Terhapus", MsgBoxStyle.Information)
        tampildata()
        kodeotomatis()
    End Sub

    Private Sub txtpncr_TextChanged(sender As Object, e As EventArgs) Handles txtpncr.TextChanged
        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "SELECT * FROM kamar WHERE id_kamar LIKE '%" & txtpncr.Text & "%' OR tipe_kamar LIKE '%" & txtpncr.Text & "%' OR jumlah_kamar LIKE '%" & txtpncr.Text & "%'"
        Dim rd As SqlDataReader = cmd.ExecuteReader
        Dim dt As New DataTable
        dt.Load(rd)
        DataGridView1.DataSource = dt
        cn.Close()
    End Sub
End Class