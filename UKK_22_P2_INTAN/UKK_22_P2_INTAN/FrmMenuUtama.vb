﻿Public Class FrmMenuUtama
    Private Sub btnberanda_Click(sender As Object, e As EventArgs) Handles btnberanda.Click
        Me.Hide()
        FrmBeranda.Show()
    End Sub

    Private Sub btnkamar_Click(sender As Object, e As EventArgs) Handles btnkamar.Click
        Dim kamar As New FrmKamar
        kamar.TopLevel = False
        Panel5.Controls.Add(kamar)
        kamar.Show()
    End Sub

    Private Sub btnfkamar_Click(sender As Object, e As EventArgs) Handles btnfkamar.Click
        Dim kamar As New FrmFasilitasKamar
        kamar.TopLevel = False
        Panel5.Controls.Add(kamar)
        kamar.Show()
    End Sub

    Private Sub btnfhotel_Click(sender As Object, e As EventArgs) Handles btnfhotel.Click
        Dim kamar As New FrmFasilitasHotel
        kamar.TopLevel = False
        Panel5.Controls.Add(kamar)
        kamar.Show()
    End Sub

    Private Sub btnpemesanan_Click(sender As Object, e As EventArgs) Handles btnpemesanan.Click
        Dim kamar As New FrmPemesanan
        kamar.TopLevel = False
        Panel5.Controls.Add(kamar)
        kamar.Show()
    End Sub

    Private Sub btnci_Click(sender As Object, e As EventArgs) Handles btnci.Click
        Dim kamar As New FrmCheckIn
        kamar.TopLevel = False
        Panel5.Controls.Add(kamar)
        kamar.Show()
    End Sub

    Private Sub btnco_Click(sender As Object, e As EventArgs) Handles btnco.Click
        Dim kamar As New FrmCheckOut
        kamar.TopLevel = False
        Panel5.Controls.Add(kamar)
        kamar.Show()
    End Sub

    Private Sub btnkeluar_Click(sender As Object, e As EventArgs)
        Me.Hide()
        FrmLogin.Show()
    End Sub
End Class