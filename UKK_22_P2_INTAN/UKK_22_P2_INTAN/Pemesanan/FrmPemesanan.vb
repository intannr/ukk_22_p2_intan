﻿Imports System.Data.SqlClient
Public Class FrmPemesanan
    Dim cn As New SqlConnection
    Dim cmd As New SqlCommand
    Sub tampildata()
        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "SELECT * FROM pemesanan"
        Dim rd As SqlDataReader = cmd.ExecuteReader
        Dim dt As New DataTable
        dt.Load(rd)
        DataGridView1.DataSource = dt
        cn.Close()

        DataGridView1.Columns(0).HeaderText = "ID Pemesanan"
        DataGridView1.Columns(1).HeaderText = "Nama Pemesan"
        DataGridView1.Columns(2).HeaderText = "No KTP"
        DataGridView1.Columns(3).HeaderText = "No HP"
        DataGridView1.Columns(4).HeaderText = "Nama Tamu"
        DataGridView1.Columns(5).HeaderText = "ID Kamar"
        DataGridView1.Columns(6).HeaderText = "Tipe Kamar"
        DataGridView1.Columns(7).HeaderText = "Tanggal Pemesanan"


        DataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill

    End Sub

    Sub bersih()
        txtidpms.Text = ""
        txtnamap.Text = ""
        txtktp.Text = ""
        txthp.Text = ""
        txtnamat.Text = ""
        txtid.Text = ""
        txttipe.Text = ""

    End Sub

    Sub kodeotomatis()
        Dim kodeauto As Single
        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "SELECT COUNT(*) AS id_pemesanan FROM pemesanan"
        Dim rd As SqlDataReader = cmd.ExecuteReader
        While rd.Read
            kodeauto = Val(rd.Item("id_pemesanan").ToString) + 1
        End While
        Select Case Len(Trim(kodeauto))
            Case 1 : txtidpms.Text = "PM00" + Trim(Str(kodeauto))
            Case 2 : txtidpms.Text = "PM-" + Trim(Str(kodeauto))
        End Select
        rd.Close()
        cn.Close()
    End Sub

    Sub id_kamar()
        Try
            Dim dt As New DataTable
            Dim ds As New DataSet
            ds.Tables.Add(dt)
            Dim da As New SqlDataAdapter("SELECT * FROM kamar", cn)
            da.Fill(dt)
            Dim r As DataRow
            txtid.AutoCompleteCustomSource.Clear()
            For Each r In dt.Rows
                txtid.AutoCompleteCustomSource.Add(r.Item(0).ToString)
            Next
        Catch ex As Exception
            cn.Close()
        End Try
    End Sub

    Private Sub FrmPemesanan_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cn.ConnectionString = "Data Source=DESKTOP-GCB5DI9\SQLEXPRESS;Initial Catalog=db_22_P2_Intan;Integrated Security=True"
        tampildata()
        txtidpms.Enabled = False
        kodeotomatis()
        id_kamar()
    End Sub

    Private Sub txtid_TextChanged(sender As Object, e As EventArgs) Handles txtid.TextChanged
        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "SELECT * FROM kamar WHERE id_kamar = '" & txtid.Text & "'"
        cmd.ExecuteNonQuery()
        Dim rd As SqlDataReader = cmd.ExecuteReader
        rd.Read()
        If rd.HasRows Then
            txttipe.Text = rd.Item("tipe_kamar")
        Else
        End If
        rd.Close()
        cn.Close()
    End Sub

    Private Sub DataGridView1_DoubleClick(sender As Object, e As EventArgs) Handles DataGridView1.DoubleClick
        txtidpms.Text = DataGridView1.SelectedCells(0).Value
        txtnamap.Text = DataGridView1.SelectedCells(1).Value
        txtktp.Text = DataGridView1.SelectedCells(2).Value
        txthp.Text = DataGridView1.SelectedCells(3).Value
        txtnamat.Text = DataGridView1.SelectedCells(4).Value
        txtid.Text = DataGridView1.SelectedCells(5).Value
        txttipe.Text = DataGridView1.SelectedCells(6).Value
        tglpms.Value = DataGridView1.SelectedCells(7).Value
    End Sub

    Private Sub btntambah_Click(sender As Object, e As EventArgs) Handles btntambah.Click
        If txtidpms.Text = "" Then
            MessageBox.Show("ID Pemesanan, tidak boleh dikosongkan")
        ElseIf txtid.Text = "" Then
            MessageBox.Show("ID kamar wajib diisi, tidak boleh dikosongkan")
        ElseIf txtidpms.Text <> "" And txtid.Text <> "" Then
            cn.Open()
            cmd.Connection = cn
            cmd.CommandText = "INSERT INTO pemesanan VALUES ('" & txtidpms.Text & "','" & txtnamap.Text & "','" & txtktp.Text & "','" & txthp.Text & "','" & txtnamat.Text & "','" & txtid.Text & "','" & txttipe.Text & "','" & tglpms.Value.ToString("yyyy-MM-dd") & "')"
            cmd.ExecuteNonQuery()
            cn.Close()
            bersih()
            MsgBox("Data Pemesanan Berhasil Tersimpan", MsgBoxStyle.Information)
            tampildata()
            kodeotomatis()
        End If
    End Sub

    Private Sub btnubah_Click(sender As Object, e As EventArgs) Handles btnubah.Click
        If txtidpms.Text = "" Then
            MessageBox.Show("ID Pemesanan, tidak boleh dikosongkan")
        ElseIf txtid.Text = "" Then
            MessageBox.Show("ID Kamar wajib diisi, tidak boleh dikosongkan")
        ElseIf txtidpms.Text <> "" And txtid.Text <> "" Then
            cn.Open()
            cmd.Connection = cn
            cmd.CommandText = "UPDATE pemesanan SET nama_pemesan ='" & txtnamap.Text & "', no_ktp ='" & txtktp.Text & "', telp ='" & txthp.Text & "', nama_tamu ='" & txtnamat.Text & "', id_kamar ='" & txtid.Text & "', tipe_kamar ='" & txttipe.Text & "', tgl_pemesanan ='" & tglpms.Value.ToString("yyyy-MM-dd") & "' WHERE id_pemesanan ='" & txtidpms.Text & "'"
            cmd.ExecuteNonQuery()
            cn.Close()
            bersih()
            MsgBox("Data Pemesanan Berhasil Terubah", MsgBoxStyle.Information)
            tampildata()
            kodeotomatis()
        End If
    End Sub

    Private Sub btnbatal_Click(sender As Object, e As EventArgs) Handles btnbatal.Click
        bersih()
        kodeotomatis()
    End Sub

    Private Sub btnhapus_Click(sender As Object, e As EventArgs) Handles btnhapus.Click
        Dim baris As Integer
        Dim id As String

        baris = DataGridView1.CurrentCell.RowIndex
        id = DataGridView1(0, baris).Value.ToString

        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "DELETE FROM pemesanan WHERE id_pemesanan ='" + id + "'"
        cmd.ExecuteNonQuery()
        cn.Close()
        MsgBox("Data Pemesanan Berhasil Terhapus", MsgBoxStyle.Information)
        tampildata()
        kodeotomatis()
    End Sub

    Private Sub txtpncr_TextChanged(sender As Object, e As EventArgs) Handles txtpncr.TextChanged
        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "SELECT * FROM pemesanan WHERE id_pemesanan LIKE '%" & txtpncr.Text & "%' OR nama_tamu LIKE '%" & txtpncr.Text & "%' OR nama_pemesan LIKE '%" & txtpncr.Text & "%'"
        Dim rd As SqlDataReader = cmd.ExecuteReader
        Dim dt As New DataTable
        dt.Load(rd)
        DataGridView1.DataSource = dt
        cn.Close()
    End Sub

    Private Sub btnlaporan_Click(sender As Object, e As EventArgs) Handles btnlaporan.Click
        Me.Hide()
        LaporanPemesanan.Show()
    End Sub

    Private Sub txthp_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txthp.KeyPress
        Dim keyascii As Short = Asc(e.KeyChar)
        If (e.KeyChar Like "[0-9]" OrElse keyascii = Keys.Back) Then
            keyascii = 0
        Else
            e.Handled = CBool(keyascii)
        End If
    End Sub

    Private Sub txtktp_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtktp.KeyPress
        Dim keyascii As Short = Asc(e.KeyChar)
        If (e.KeyChar Like "[0-9]" OrElse keyascii = Keys.Back) Then
            keyascii = 0
        Else
            e.Handled = CBool(keyascii)
        End If
    End Sub
End Class