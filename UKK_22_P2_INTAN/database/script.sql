USE [master]
GO
/****** Object:  Database [db_22_P2_Intan]    Script Date: 5/11/2022 11:58:04 AM ******/
CREATE DATABASE [db_22_P2_Intan]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'db_22_P2', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\db_22_P2.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'db_22_P2_log', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\db_22_P2_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [db_22_P2_Intan] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [db_22_P2_Intan].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [db_22_P2_Intan] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [db_22_P2_Intan] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [db_22_P2_Intan] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [db_22_P2_Intan] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [db_22_P2_Intan] SET ARITHABORT OFF 
GO
ALTER DATABASE [db_22_P2_Intan] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [db_22_P2_Intan] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [db_22_P2_Intan] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [db_22_P2_Intan] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [db_22_P2_Intan] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [db_22_P2_Intan] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [db_22_P2_Intan] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [db_22_P2_Intan] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [db_22_P2_Intan] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [db_22_P2_Intan] SET  DISABLE_BROKER 
GO
ALTER DATABASE [db_22_P2_Intan] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [db_22_P2_Intan] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [db_22_P2_Intan] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [db_22_P2_Intan] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [db_22_P2_Intan] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [db_22_P2_Intan] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [db_22_P2_Intan] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [db_22_P2_Intan] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [db_22_P2_Intan] SET  MULTI_USER 
GO
ALTER DATABASE [db_22_P2_Intan] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [db_22_P2_Intan] SET DB_CHAINING OFF 
GO
ALTER DATABASE [db_22_P2_Intan] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [db_22_P2_Intan] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [db_22_P2_Intan] SET DELAYED_DURABILITY = DISABLED 
GO
USE [db_22_P2_Intan]
GO
/****** Object:  Table [dbo].[check_in]    Script Date: 5/11/2022 11:58:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[check_in](
	[id_checkin] [char](10) NOT NULL,
	[id_pemesanan] [char](10) NULL,
	[nama_tamu] [varchar](20) NULL,
	[check_in] [date] NULL,
 CONSTRAINT [PK_check_in_1] PRIMARY KEY CLUSTERED 
(
	[id_checkin] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[check_out]    Script Date: 5/11/2022 11:58:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[check_out](
	[id_checkout] [char](10) NOT NULL,
	[id_checkin] [char](10) NULL,
	[nama_tamu] [varchar](20) NULL,
	[check_in] [date] NULL,
	[check_out] [date] NULL,
	[jumlah_hari] [int] NULL,
 CONSTRAINT [PK_check_out] PRIMARY KEY CLUSTERED 
(
	[id_checkout] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[fasilitas_hotel]    Script Date: 5/11/2022 11:58:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fasilitas_hotel](
	[id_fasilitashotel] [char](10) NOT NULL,
	[nama_fasilitas] [varchar](20) NULL,
	[keterangan] [varchar](100) NULL,
 CONSTRAINT [PK_fasilitas_hotel] PRIMARY KEY CLUSTERED 
(
	[id_fasilitashotel] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[fasilitas_kamar]    Script Date: 5/11/2022 11:58:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fasilitas_kamar](
	[id_fasilitas] [char](10) NOT NULL,
	[id_kamar] [char](10) NULL,
	[tipe_kamar] [varchar](20) NULL,
	[fasilitas_kamar] [varchar](100) NULL,
 CONSTRAINT [PK_fasilitas_kamar] PRIMARY KEY CLUSTERED 
(
	[id_fasilitas] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[kamar]    Script Date: 5/11/2022 11:58:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[kamar](
	[id_kamar] [char](10) NOT NULL,
	[tipe_kamar] [varchar](20) NULL,
	[jumlah_kamar] [int] NULL,
 CONSTRAINT [PK_kamar] PRIMARY KEY CLUSTERED 
(
	[id_kamar] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[login]    Script Date: 5/11/2022 11:58:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[login](
	[username] [varchar](20) NOT NULL,
	[password] [varchar](20) NULL,
	[hak] [varchar](20) NULL,
 CONSTRAINT [PK_login] PRIMARY KEY CLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pemesanan]    Script Date: 5/11/2022 11:58:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pemesanan](
	[id_pemesanan] [char](10) NOT NULL,
	[nama_pemesan] [varchar](20) NULL,
	[no_ktp] [char](16) NULL,
	[telp] [char](12) NULL,
	[nama_tamu] [varchar](20) NULL,
	[id_kamar] [char](10) NULL,
	[tipe_kamar] [varchar](20) NULL,
	[tgl_pemesanan] [date] NULL,
 CONSTRAINT [PK_pemesanan] PRIMARY KEY CLUSTERED 
(
	[id_pemesanan] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[check_in] ([id_checkin], [id_pemesanan], [nama_tamu], [check_in]) VALUES (N'CI001     ', N'PM001     ', N'Ayu', CAST(N'2022-05-07' AS Date))
INSERT [dbo].[check_out] ([id_checkout], [id_checkin], [nama_tamu], [check_in], [check_out], [jumlah_hari]) VALUES (N'CO001     ', N'CI001     ', N'Ayu', CAST(N'2022-05-07' AS Date), CAST(N'2022-05-10' AS Date), 3)
INSERT [dbo].[fasilitas_hotel] ([id_fasilitashotel], [nama_fasilitas], [keterangan]) VALUES (N'FH01      ', N'Kolam Renang', N'Tempat bersantai bersama keluarga')
INSERT [dbo].[fasilitas_hotel] ([id_fasilitashotel], [nama_fasilitas], [keterangan]) VALUES (N'FH02      ', N'SPA', N'Bersantai sambil memanjakan diri')
INSERT [dbo].[fasilitas_kamar] ([id_fasilitas], [id_kamar], [tipe_kamar], [fasilitas_kamar]) VALUES (N'FK01      ', N'KMR01     ', N'Standard', N'AC ')
INSERT [dbo].[fasilitas_kamar] ([id_fasilitas], [id_kamar], [tipe_kamar], [fasilitas_kamar]) VALUES (N'FK02      ', N'KMR02     ', N'Superior', N'AC TV23inc')
INSERT [dbo].[fasilitas_kamar] ([id_fasilitas], [id_kamar], [tipe_kamar], [fasilitas_kamar]) VALUES (N'FK03      ', N'KMR03     ', N'Deluxe', N'WIFI AC TV')
INSERT [dbo].[kamar] ([id_kamar], [tipe_kamar], [jumlah_kamar]) VALUES (N'KMR01     ', N'Standard', 4)
INSERT [dbo].[kamar] ([id_kamar], [tipe_kamar], [jumlah_kamar]) VALUES (N'KMR02     ', N'Superior', 3)
INSERT [dbo].[kamar] ([id_kamar], [tipe_kamar], [jumlah_kamar]) VALUES (N'KMR03     ', N'Deluxe', 5)
INSERT [dbo].[login] ([username], [password], [hak]) VALUES (N'admin', N'admin', N'Admin')
INSERT [dbo].[login] ([username], [password], [hak]) VALUES (N'intan', N'123', N'Resepsionis')
INSERT [dbo].[pemesanan] ([id_pemesanan], [nama_pemesan], [no_ktp], [telp], [nama_tamu], [id_kamar], [tipe_kamar], [tgl_pemesanan]) VALUES (N'PM001     ', N'Ayu Rahma', N'0000232344546778', N'081256768123', N'Ayu', N'KMR01     ', N'Standard', CAST(N'2022-04-28' AS Date))
ALTER TABLE [dbo].[check_in]  WITH CHECK ADD  CONSTRAINT [FK_check_in_pemesanan] FOREIGN KEY([id_pemesanan])
REFERENCES [dbo].[pemesanan] ([id_pemesanan])
GO
ALTER TABLE [dbo].[check_in] CHECK CONSTRAINT [FK_check_in_pemesanan]
GO
ALTER TABLE [dbo].[check_out]  WITH CHECK ADD  CONSTRAINT [FK_check_out_check_in] FOREIGN KEY([id_checkin])
REFERENCES [dbo].[check_in] ([id_checkin])
GO
ALTER TABLE [dbo].[check_out] CHECK CONSTRAINT [FK_check_out_check_in]
GO
ALTER TABLE [dbo].[fasilitas_kamar]  WITH CHECK ADD  CONSTRAINT [FK_fasilitas_kamar_kamar] FOREIGN KEY([id_kamar])
REFERENCES [dbo].[kamar] ([id_kamar])
GO
ALTER TABLE [dbo].[fasilitas_kamar] CHECK CONSTRAINT [FK_fasilitas_kamar_kamar]
GO
ALTER TABLE [dbo].[pemesanan]  WITH CHECK ADD  CONSTRAINT [FK_pemesanan_kamar] FOREIGN KEY([id_kamar])
REFERENCES [dbo].[kamar] ([id_kamar])
GO
ALTER TABLE [dbo].[pemesanan] CHECK CONSTRAINT [FK_pemesanan_kamar]
GO
USE [master]
GO
ALTER DATABASE [db_22_P2_Intan] SET  READ_WRITE 
GO
